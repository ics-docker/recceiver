FROM python:3.10.7 as build

ARG RECCEIVER_VERSION=1.6

RUN useradd -ms /bin/bash recceiver
USER recceiver
WORKDIR /home/recceiver

RUN git clone --depth 1 --branch $RECCEIVER_VERSION https://github.com/ChannelFinder/recsync.git

WORKDIR /home/recceiver/recsync
COPY patches/expandvars.patch patches/pollreactor.patch patches/cfstore.patch patches/channelowner.patch ./
RUN git apply expandvars.patch pollreactor.patch cfstore.patch channelowner.patch

WORKDIR /home/recceiver
COPY requirements.txt requirements.txt

RUN python -m venv venv && \
    venv/bin/pip install \
        --no-cache-dir \
        -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple \
        -r requirements.txt

RUN venv/bin/python -m pip install ./recsync/server

FROM python:3.10.7-slim

RUN useradd -ms /bin/bash recceiver
USER recceiver
WORKDIR /home/recceiver

ENV RECCEIVER_ADDRLIST=255.255.255.255:5049 \
    RECCEIVER_PROCS=cf \
    RECCEIVER_ANNOUNCE_INTERVAL=15.0 \
    RECCEIVER_TCP_TIMEOUT=15.0 \
    RECCEIVER_COMMIT_INTERVAL=5.0 \
    RECCEIVER_MAX_ACTIVE=20 \
    RECCEIVER_LOGLEVEL=WARN \
    CHANNELFINDER_URL=http://channelfinder \
    CHANNELFINDER_USERNAME=channelfinder \
    CHANNELFINDER_PASSWORD=1234 \
    CHANNELFINDER_INFOTAGS=""

COPY --from=build /home/recceiver/venv venv
COPY docker.conf docker.conf

CMD venv/bin/twistd --pidfile= --nodaemon recceiver --config docker.conf
